-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 22-10-2020 a las 04:12:02
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `proyecto_barber`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `barbershop`
--

CREATE TABLE `barbershop` (
  `id_barbershop` int(11) NOT NULL,
  `name_barbershop` varchar(100) NOT NULL,
  `id_user` int(11) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `location` varchar(100) DEFAULT NULL,
  `logo` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `barbershop`
--

INSERT INTO `barbershop` (`id_barbershop`, `name_barbershop`, `id_user`, `owner`, `location`, `logo`) VALUES
(1, 'Marcial Barbershop', 21, 'Alexander Vladimir Gil Sanchez', NULL, NULL),
(3, 'Juan Barbershop', 23, 'Juan Ramirez', NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category_user`
--

CREATE TABLE `category_user` (
  `id_category` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `category` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `category_user`
--

INSERT INTO `category_user` (`id_category`, `id_user`, `category`) VALUES
(1, 21, 'Barbero'),
(2, 22, 'Cliente'),
(3, 23, 'Barbero'),
(4, 24, 'Barbero'),
(5, 25, 'Cliente'),
(6, 26, 'Cliente'),
(7, 27, 'Cliente');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client_barbershop`
--

CREATE TABLE `client_barbershop` (
  `id_client_barbershop` int(11) NOT NULL,
  `id_barbershop` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name_barbershop` varchar(100) NOT NULL,
  `client_name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `client_barbershop`
--

INSERT INTO `client_barbershop` (`id_client_barbershop`, `id_barbershop`, `id_user`, `name_barbershop`, `client_name`, `email`) VALUES
(1, 3, 26, 'Juan Barbershop', 'Kevin Melo', 'kevinmeol@gmail.com'),
(2, 3, 27, 'Juan Barbershop', 'Jose Ramon Diaz', 'josejose@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client_room`
--

CREATE TABLE `client_room` (
  `id_client_room` int(11) NOT NULL,
  `id_room` int(11) NOT NULL,
  `id_barbershop` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `date_time` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `employee_barbershop`
--

CREATE TABLE `employee_barbershop` (
  `id_employee` int(11) NOT NULL,
  `id_barbershop` int(11) NOT NULL,
  `id_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `employee_barbershop`
--

INSERT INTO `employee_barbershop` (`id_employee`, `id_barbershop`, `id_user`) VALUES
(1, 3, 24);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `room`
--

CREATE TABLE `room` (
  `id_room` int(11) NOT NULL,
  `id_barbershop` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name_barbershop` varchar(100) NOT NULL,
  `owner` varchar(100) NOT NULL,
  `employee` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `date_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `room`
--

INSERT INTO `room` (`id_room`, `id_barbershop`, `id_user`, `name_barbershop`, `owner`, `employee`, `description`, `date_time`) VALUES
(15, 3, 23, 'Juan Barbershop', 'Juan Ramirez', 'NULL', 'prueba entry', '23:15:54');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id_user` int(11) NOT NULL,
  `user_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(155) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` longblob DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id_user`, `user_name`, `full_name`, `email`, `password`, `photo`) VALUES
(21, 'AlexGil2022', 'Alexander Vladimir Gil Sanchez', 'alexgil2022@gmail.com', '$2y$10$coPjVDHnI7KC0gGqZTfRX.Oqg613N1awFaFYUy71D3CjXmA0cUeFW', NULL),
(22, 'Felipito27', 'Felipe Tercero', 'felipito27@gmail.com', '$2y$10$7x1L0UTz91x9foA0xemuO.Pxbr6JxTjagdVIbRJmGaTiaE6uPF6AK', NULL),
(23, 'JuanRa', 'Juan Ramirez', 'juanra@gmail.com', '$2y$10$C07xPmiryE/lwZKKr.verOUATqObOUZsMQc9MxY8g9E8nXdmAT7B.', NULL),
(24, 'Johan001', 'Johan Love', 'johanloeve@gmail.com', '$2y$10$N5hJ80Lf5Uuvgc3nz2Yn6emuYvbB1GcQVfnJwvSlMCn5lwEJzR1wy', NULL),
(25, 'El pepe', 'Pepe Cadena', 'elpepe01@gmail.com', '$2y$10$rMEUMAO80cX4dueAcQMkoO5dwLAu0x/78GhdSwHH7K26HrgO4Qwe.', NULL),
(26, 'KevinMelo01', 'Kevin Melo', 'kevinmeol@gmail.com', '$2y$10$kN5Nwe5Dg0fT3ymARsSacup3.4CMuevBOf82986meJYOOX5htQ.f6', NULL),
(27, 'JoseMon', 'Jose Ramon Diaz', 'josejose@gmail.com', '$2y$10$zQzGoY7rPHuM420QGUKZkOSaZ9.w1ECMjB4VBsWifXJvaJ/ZAXnvO', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `barbershop`
--
ALTER TABLE `barbershop`
  ADD PRIMARY KEY (`id_barbershop`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `category_user`
--
ALTER TABLE `category_user`
  ADD PRIMARY KEY (`id_category`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `client_barbershop`
--
ALTER TABLE `client_barbershop`
  ADD PRIMARY KEY (`id_client_barbershop`),
  ADD KEY `id_barbershop` (`id_barbershop`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `client_room`
--
ALTER TABLE `client_room`
  ADD PRIMARY KEY (`id_client_room`),
  ADD KEY `id_room` (`id_room`),
  ADD KEY `id_barbershop` (`id_barbershop`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `employee_barbershop`
--
ALTER TABLE `employee_barbershop`
  ADD PRIMARY KEY (`id_employee`),
  ADD KEY `id_barbershop` (`id_barbershop`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id_room`),
  ADD KEY `id_barbershop` (`id_barbershop`),
  ADD KEY `id_user` (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `barbershop`
--
ALTER TABLE `barbershop`
  MODIFY `id_barbershop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `category_user`
--
ALTER TABLE `category_user`
  MODIFY `id_category` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `client_barbershop`
--
ALTER TABLE `client_barbershop`
  MODIFY `id_client_barbershop` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `client_room`
--
ALTER TABLE `client_room`
  MODIFY `id_client_room` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `employee_barbershop`
--
ALTER TABLE `employee_barbershop`
  MODIFY `id_employee` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `room`
--
ALTER TABLE `room`
  MODIFY `id_room` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `barbershop`
--
ALTER TABLE `barbershop`
  ADD CONSTRAINT `barbershop_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `category_user`
--
ALTER TABLE `category_user`
  ADD CONSTRAINT `category_user_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `client_barbershop`
--
ALTER TABLE `client_barbershop`
  ADD CONSTRAINT `client_barbershop_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `client_barbershop_ibfk_2` FOREIGN KEY (`id_barbershop`) REFERENCES `barbershop` (`id_barbershop`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `client_room`
--
ALTER TABLE `client_room`
  ADD CONSTRAINT `client_room_ibfk_1` FOREIGN KEY (`id_room`) REFERENCES `room` (`id_room`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `client_room_ibfk_2` FOREIGN KEY (`id_barbershop`) REFERENCES `barbershop` (`id_barbershop`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `client_room_ibfk_3` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `employee_barbershop`
--
ALTER TABLE `employee_barbershop`
  ADD CONSTRAINT `employee_barbershop_ibfk_1` FOREIGN KEY (`id_barbershop`) REFERENCES `barbershop` (`id_barbershop`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `employee_barbershop_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `room`
--
ALTER TABLE `room`
  ADD CONSTRAINT `room_ibfk_1` FOREIGN KEY (`id_barbershop`) REFERENCES `barbershop` (`id_barbershop`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `room_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id_user`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
