<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    function msg($success,$status,$message,$extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    require __DIR__.'/../Class/database.php';
    require __DIR__.'/../Class/JwtHandler.php';

    $db_connection = new database();
    $conn = $db_connection->dbConnection();

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    if($_SERVER["REQUEST_METHOD"] != "POST"):
        $returnData = msg(0, 404, 'Pagina no encontrada!');

    elseif(!isset($data->email)
        || !isset($data->password)
        || empty($data->email)
        || empty($data->password)
    ):
        $fields = ['fields' => ['email', 'password']];
        $returnData = msg(0, 422, 'Ingresa los campos que te piden', $fields);

    else:
        $email = trim($data->email);
        $password = trim($data->password);
        
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            $returnData = msg(0, 422, 'El email que ha ingresado no es correcto.');
        
        elseif(strlen($password) < 8):
            $returnData = msg(0, 422, 'La clave que ha ingresado es muy corta!');

        else:
            try{
                $fetch_user_by_email = "SELECT * FROM `users` WHERE `email` = :email";
                $query_stmt = $conn->prepare($fetch_user_by_email);
                $query_stmt->bindValue(':email', $email, PDO::PARAM_STR);
                $query_stmt->execute();

                if($query_stmt->rowCount()):
                    $row = $query_stmt->fetch(PDO::FETCH_ASSOC);
                    $check_password = password_verify($password, $row['password']);

                    if($check_password):
                        $jwt = new JwtHandler();
                        $token = $jwt->_jwt_encode_data(
                            'http://localhost/Proyecto/Security/Barber-Info/',
                            array("id_user"=>$row['id_user'])
                        );

                        $returnData = [
                            'success' => 1,
                            'message' => 'Te has logueado correctamente.',
                            'token' => $token
                        ];

                    else:
                        $returnData = msg(0, 422, 'Clave Invalida');
                    endif;

                else:
                    $returnData = msg(0, 422, 'Correo Invalido!');
                endif;

            } catch(PDOException $e){
                $returnData = msg(0, 500, $e->getMessage());
            }
        endif;
    endif;

    echo json_encode($returnData);
?>