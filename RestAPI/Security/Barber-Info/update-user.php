<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Class/database.php';
    require __DIR__.'/../middlewares/Auth.php';

    $allHeaders = getallheaders();
    $db_connection = new database();
    $conn = $db_connection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }


    if($auth->isAuth()) {
        $returnData = $auth->isAuth();

        $idUser = trim(json_encode($returnData['user']['id_user']), "\"..\"");

        if(!isset($data->full_name)
            || !isset($data->email)
            || !isset($data->password)
            || !isset($data->user_name)
            
            || empty($data->full_name)
            || empty($data->email)
            || empty($data->password)
            || empty($data->user_name)
        ):
            $fields = ['fields' => ['user_name', 'full_name', 'email', 'password']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        
        else:
            $username = trim($data->user_name);
            $fullName = trim($data->full_name);
            $email = trim($data->email);
            $password = trim($data->password);

            if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
                $returnData = msg(0, 422, 'El email que ha ingresado no es correcto.');
            
            elseif(strlen($password) < 8):
                $returnData = msg(0, 422, 'La clave que ha ingresado es muy corta!');
            
            elseif(strlen($fullName) < 5):
                $returnData = msg(0, 422, 'El Nombre que ha ingresado es muy corto');

            elseif(strlen($username) < 3):
                $returnData = msg(0, 422, 'El Nombre de usuario que ha ingresado es muy corto');

            else:
                try{
                    // Valida que el email no exista

                    $check_email = "SELECT `email` FROM `users` WHERE `email` = :email";
                    $check_email_stmt = $conn->prepare($check_email);
                    $check_email_stmt->bindValue(':email', $email, PDO::PARAM_STR);
                    $check_email_stmt->execute();

                    if($check_email_stmt->rowCount()):
                        $returnData = msg(0,422, 'El email que ha ingresado ya esta en uso!');

                    else:
                        // Valida que username no exista

                        $check_username = "SELECT `user_name` FROM `users` WHERE `user_name` = :username";
                        $check_username_stmt = $conn->prepare($check_username);
                        $check_username_stmt->bindValue(':username', $username, PDO::PARAM_STR);
                        $check_username_stmt->execute();

                        if($check_username_stmt->rowCount()):
                            $returnData = msg(0,422, 'El nombre de usuario que ha ingresado ya esta en uso!');

                        else:
                            // Valida si el usuario es un barbero
                            $check_user = "SELECT * FROM `barbershop` WHERE `id_user` = :idUser";
                            $check_user_stmt = $conn->prepare($check_user);

                            $check_user_stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);
                            $check_user_stmt->execute();

                            if($check_user_stmt->rowCount()):
                                $update_query = "UPDATE `users` SET `user_name` = :username, `full_name` = :fullName, `email` = :email, `password` = :password WHERE `id_user` = :idUser";
                                $update_query_stmt = $conn->prepare($update_query);

                                $update_query_stmt->bindValue(':username', $username, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':fullName', $fullName, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':email', $email, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':password', password_hash($password, PASSWORD_DEFAULT), PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);

                                $update_query_stmt->execute();

                                // Actualiza tambien la informacion de la barberia

                                $update_query_barbershop = "UPDATE `barbershop` SET `owner` = :owner WHERE `id_user` = :id";
                                $update_query_barbershop_stmt = $conn->prepare($update_query_barbershop);
                           
                                $update_query_barbershop_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
                                $update_query_barbershop_stmt->bindValue(':owner', $fullName, PDO::PARAM_STR);

                                $update_query_barbershop_stmt->execute();

                                $returnData = msg(1,201, 'Toda la informacion ha sido actualizada correctamente!');
                            
                            else:
                                $update_query = "UPDATE `users` SET `user_name` = :username, `full_name` = :fullName, `email` = :email, `password` = :password WHERE `id_user` = :idUser";
                                $update_query_stmt = $conn->prepare($update_query);

                                $update_query_stmt->bindValue(':username', $username, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':fullName', $fullName, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':email', $email, PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':password', password_hash($password, PASSWORD_DEFAULT), PDO::PARAM_STR);
                                $update_query_stmt->bindValue(':idUser', $idUser, PDO::PARAM_INT);

                                $update_query_stmt->execute();

                                $returnData = msg(1,201, 'La informacion del usuario ha sido actualizada correctamente!');

                            endif;

                        endif;

                    endif;

                } catch(PDOException $e) {
                    $returnData = msg(0, 500,$e->getMessage());
                }

            endif;
        
        endif;

    } else {
        $returnData = [
            'success' => 0,
            'status' => 401,
            'message' => "No autorizado"
        ];
    }

    echo json_encode($returnData);
?>