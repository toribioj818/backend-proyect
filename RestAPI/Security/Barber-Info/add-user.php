<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    // Database
    require __DIR__.'/../Class/database.php';
    require '../../db_connection.php';
    $db_connection = new database();
    $conn = $db_connection->dbConnection();

    // Data
    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    // Request Method
    if($_SERVER["REQUEST_METHOD"] != "POST"):
        $returnData = msg(0,404,'Page Not Found!');

    // Validate empty
    elseif(!isset($data->user_name)
        || !isset($data->full_name)
        || !isset($data->email)
        || !isset($data->password)
        || !isset($data->category)

        || empty($data->user_name)
        || empty($data->full_name)
        || empty($data->email)
        || empty($data->password)
        || empty($data->category)
    ):

        $fields = ['fields' => ['user_name', 'full_name', 'email', 'password', 'category']];
        $returnData = msg(0, 402, 'Please Fill in all Required Fields', $fields);
    else:
        $username = trim($data->user_name);
        $full_name = trim($data->full_name);
        $email = trim($data->email);
        $password = trim($data->password);
        $category = trim($data->category);

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)):
            $returnData = msg(0, 422, 'El email que ha ingresado es invalido!');

        elseif(strlen($password) < 8):
            $returnData = msg(0, 422, 'La clave que ha ingresado es muy corta!');

        elseif(strlen($username) < 3):
            $returnData = msg(0, 422, 'El nombre de usuario que ha ingresado es muy corto');

        elseif(strlen($full_name) < 5):
            $returnData = msg(0, 422, 'El Nombre que ha ingresado es muy corto');

        elseif($category != "Barbero" && $category != "Cliente"):
            $returnData = msg(0, 422, 'La categoria que ha ingresado no es la correcta');
        
        else:
            try{
                $check_email = "SELECT `email` FROM `users` WHERE `email` = :email";
                $check_email_stmt = $conn->prepare($check_email);
                $check_email_stmt->bindValue(':email', $email, PDO::PARAM_STR);
                $check_email_stmt->execute();

                if($check_email_stmt->rowCount()):
                    $returnData = msg(0,422, 'El email que ha ingresado ya esta en uso!');

                else:
                    $insert_query = "INSERT INTO `users` (`user_name`, `full_name`, `email`, `password`) VALUES(:user_name, :full_name, :email, :password)";
                    $insert_stmt = $conn->prepare($insert_query);

                    $insert_stmt->bindValue(':user_name', htmlspecialchars(strip_tags($username)), PDO::PARAM_STR);
                    $insert_stmt->bindValue(':full_name', htmlspecialchars(strip_tags($full_name)), PDO::PARAM_STR);
                    $insert_stmt->bindValue(':email', $email,PDO::PARAM_STR);
                    $insert_stmt->bindValue(':password', password_hash($password, PASSWORD_DEFAULT), PDO::PARAM_STR);

                    $insert_stmt->execute();

                    // Obtener el ultimo id
                    $last_id = $conn->lastInsertId();

                    // Insertar categoria
                    $insert_relationship_query = "INSERT INTO `category_user` (`id_user`, `category`) VALUES (:id_user, :category)";
                    $insert_relationship_stmt = $conn->prepare($insert_relationship_query);

                    $insert_relationship_stmt->bindValue(':id_user', $last_id, PDO::PARAM_STR);
                    $insert_relationship_stmt->bindValue(':category', $category, PDO::PARAM_STR);

                    $insert_relationship_stmt->execute();

                    $returnData = msg(1, 201, 'Se ha registrado correctamente.');
                
                endif;

            } catch(PDOException $e) {
                $returnData = msg(0, 500,$e->getMessage());
            }

        endif;

    endif;

    echo json_encode($returnData);

?>