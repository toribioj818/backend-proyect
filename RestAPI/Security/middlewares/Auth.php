<?php
    require __DIR__.'/../Class/JwtHandler.php';

    class Auth extends JwtHandler{
        protected $db;
        protected $headers;
        protected $token;
        
        public function __construct($db, $headers)
        {
            parent::__construct();
            $this->db = $db;
            $this->headers = $headers;
        }

        public function isAuth() {
            if(array_key_exists('Authorization',$this->headers) && !empty(trim($this->headers['Authorization']))):
                
                $this->token = explode(" ", trim($this->headers['Authorization']));
                
                if(isset($this->token[1]) && !empty(trim($this->token[1]))):
                    $data = $this->_jwt_decode_data($this->token[1]);
                    
                    if(isset($data['auth']) && isset($data['data']->id_user) && $data['auth']):
                        $user = $this->fetchUser($data['data']->id_user);
                        return $user;
                    
                    else:
                        return null;
                    
                    endif;

                else: 
                    return null;
                
                endif;

            else:
                return null;

            endif;
        }

        protected function fetchUser($user_id){
            try{
                $fetch_user_by_id = "SELECT `id_user`,`user_name`, `full_name`, `email` FROM `users` WHERE `id_user` = :id";
                $query_stmt = $this->db->prepare($fetch_user_by_id);
                $query_stmt->bindValue(':id',$user_id,PDO::PARAM_INT);
                $query_stmt->execute();

                // Informacion de la categoria del usuario

                $fetch_category_by_id = "SELECT `category` FROM `category_user` WHERE `id_user` = :id_u";
                $category_query_stmt = $this->db->prepare($fetch_category_by_id);
                $category_query_stmt->bindValue(':id_u', $user_id, PDO::PARAM_INT);
                $category_query_stmt->execute();

                if($query_stmt->rowCount() && $category_query_stmt->rowCount()):
                    $row = $query_stmt->fetch(PDO::FETCH_ASSOC);
                    $category_row = $category_query_stmt->fetch(PDO::FETCH_ASSOC);
                    return [
                        'success' => 1,
                        'status' => 200,
                        'user' => $row,
                        'category_user' => $category_row
                    ];

                else:
                    return null;
                endif;

            } catch(PDOException $e){
                return null;
            }
        }
    }
?>