<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()) {
        // Obtener info de usurio en sesion
        $response = $auth->isAuth();

        $id_user = trim(json_encode($response['user']['id_user']), "\"..\"");
        $full_name = trim(json_encode($response['user']['full_name']), "\"..\"");
        $category = trim(json_encode($response['category_user']['category']), "\"..\"");

        // Request Method
        if($_SERVER["REQUEST_METHOD"] != "POST"):
            $returnData = msg(0,404,'Pagina no autorizada!');

        elseif($category != "Barbero"):
            $returnData = msg(0,401,'Acceso no autorizado!');
        
        elseif(!isset($data->name_barbershop) || empty($data->name_barbershop)):
            $fields = ['fields' => ['name_barbershop']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        
        else:
            $nameBarbershop = $data->name_barbershop;

            if(strlen($nameBarbershop) < 5):
                $returnData = msg(0, 422, 'Debe de ingresar un nombre mas largo');
            
            else:
                try{
                    // Validar que el nombre no este registrado
                    $check_barbershop = "SELECT `name_barbershop` FROM `barbershop` WHERE `name_barbershop` = :nameBarbershop";
                    $check_barbershop_stmt = $conn->prepare($check_barbershop);
                    $check_barbershop_stmt->bindValue(':nameBarbershop', $nameBarbershop, PDO::PARAM_STR);
                    $check_barbershop_stmt->execute(); 

                    if($check_barbershop_stmt->rowCount()):
                        $returnData = msg(0,422, 'El nombre de la barberia que estas usando, ya esta registrado!');
                    
                    else:
                        $insert_query = "INSERT INTO `barbershop` (`name_barbershop`, `id_user`, `owner`, `location`) VALUES (:nameBarbershop, :idUser, :owner, null)";
                        $insert_query_stmt = $conn->prepare($insert_query);
                        $insert_query_stmt->bindValue(':nameBarbershop', $nameBarbershop, PDO::PARAM_STR);
                        $insert_query_stmt->bindValue(':idUser', $id_user, PDO::PARAM_INT);
                        $insert_query_stmt->bindValue(':owner', $full_name, PDO::PARAM_STR);
                        $insert_query_stmt->execute();

                        $last_id = $conn->lastInsertId();

                        $returnData = msg(1,201, 'La barberia se ha registrado correctamente!');

                    endif;

                } catch(PDOException $e) {
                    $returnData = msg(0, 500,$e->getMessage());
                }

            endif;
            
        endif;
    }

    echo json_encode($returnData);
?>