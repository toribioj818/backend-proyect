<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $db_connection = new database();
    $conn = $db_connection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()){

      // Informacion de usuario en sesion
      $returnData = $auth->isAuth();
      $idUser = trim(json_encode($returnData['user']['id_user']), "\"..\"");
      $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

      // Validar que el usuario tenga el permiso de actualizar
      if($category != "Barbero"):
        $returnData = msg(0,401,'Usuario no autorizado!');
      else:
        $query = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
        $query_stmt = $conn->prepare($query);
        $query_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
        $query_stmt->execute();

        if($query_stmt->rowCount()):
          // Valida que lo que manda el Usuario

          if(!isset($data->name_barbershop)
              || !isset($data->location)

              || empty($data->name_barbershop)
              || empty($data->location)
          ):
            $fields = ['fields' => ['name_barbershop', 'location']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
          else:
            $nameBarbershop = trim($data->name_barbershop);
            $location = null;

            if(strlen($nameBarbershop) < 5):
              $returnData = msg(0,401,"El nombre ingresado es muy corto!");
            else:
              try{
                $update_query = "UPDATE `barbershop` SET `name_barbershop` = :name WHERE `id_user` = :id";
                $update_query_stmt = $conn->prepare($update_query);
                $update_query_stmt->bindValue(':name', $nameBarbershop, PDO::PARAM_STR);
                $update_query_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
                $update_query_stmt->execute();

                $returnData = msg(1,201, "Informacion actualizada correctamente.");
                
              } catch(PDOException $e) {
                  $returnData = msg(0,401,$e->getMessage());
              }

            endif;

          endif;

        else:
          $returnData = msg(0,401, "No tienes ninguna barberia que editar");

        endif;

      endif;
    } else {
      $returnData = msg(0,401,"Usuario no autorizado!");
    }

    echo json_encode($returnData);
 ?>
