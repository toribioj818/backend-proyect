<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $db_connection = new database();
    $conn = $db_connection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()) {
      $returnData = $auth->isAuth();
      $idUser = trim(json_encode($returnData['user']['id_user']), "\"..\"");
      $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

      if($category != "Barbero"):
        $returnData = msg(0,401, "No autorizado!");
      else:
        $check_owner = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
        $check_owner_stmt = $conn->prepare($check_owner);
        $check_owner_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
        $check_owner_stmt->execute();

        if($check_owner_stmt->rowCount()):

          try {
            $delete_query = "DELETE FROM `barbershop` WHERE `id_user` = :id";
            $delete_query_stmt = $conn->prepare($delete_query);
            $delete_query_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
            $delete_query_stmt->execute();

            $returnData = msg(1,201,"La barberia se ha eliminado correctamente!");

          } catch(PDOException $e) {
            $returnData = msg(0,401,$e->getMessage());
          }

        else:
          $returnData = msg(0,401,"Actualmente no tienes una barberia!");
        endif;

      endif;

    } else {
      $returnData = msg(0,401,"No autorizado!");
    }

    echo json_encode($returnData);
 ?>
