<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $db_connection = new database();
    $conn = $db_connection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $returnData = [
        'success' => 0,
        'status' => 401,
        'message' => "No autorizado"
    ];

    if($auth->isAuth()){
        $returnData = $auth->isAuth();

        try {
            $query = "SELECT * FROM `barbershop`";
            $query_stmt = $conn->prepare($query);
            $query_stmt->execute();

            $row = $query_stmt->fetch(PDO::FETCH_ASSOC);

            $returnData = [
                "success" => 1,
                "status" => 201,
                "barbershops" => $row
            ];

        } catch(PDOException $e) {
            $returnData = msg(0, 500,$e->getMessage());
        }
        
    }

    echo json_encode($returnData);
?>