<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../../Security/Class/database.php';
    require __DIR__.'/../../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()):
        $returnData = $auth->isAuth();

        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
        $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

        if($category != "Barbero"):
            $returnData = msg(0,401,"No autorizado!");
        elseif(!isset($data->id_client_room) || empty($data->id_client_room)):
            $fields = ['fields' => ['id_client_room']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);

        else:
            $idClientRoom = $data->id_client_room;

            try {
                // Valida que tenga una sala
                $check_user = "SELECT * FROM `room` WHERE `id_user` = :id";
                $check_user_stmt = $conn->prepare($check_user);
                $check_user_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                $check_user_stmt->execute();

                if($check_user_stmt->rowCount()):
                    // Remueve el turno
                    $query = "DELETE FROM `client_room` WHERE `id_client_room` = :id";
                    $query_stmt = $conn->prepare($query);
                    $query_stmt->bindValue(':id', $idClientRoom, PDO::PARAM_INT);
                    $query_stmt->execute();

                    $returnData = msg(1,201,"El cliente ha sido despachado correctamente!");
                else:
                    $returnData = msg(0,401,"No tienes una sala creada!");
                endif;
            } catch(PDOException $e){
                $returnData = msg(0,401,$e->getMessage());
            }
        endif;

    else:
        $returnData = msg(0,401,"No autorizado!");
    endif;


    echo json_encode($returnData);
?>