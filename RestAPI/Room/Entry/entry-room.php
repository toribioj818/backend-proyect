<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../../Security/Class/database.php';
    require __DIR__.'/../../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()):
        $returnData = $auth->isAuth();

        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
        $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

        $date = date("H:i:s");


        if(!isset($data->id_room) || empty($data->id_room)):
            $fields = ['fields' => ['id_room']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        else:
            $id_room = $data->id_room;

            try {

                // Verifica que no este en sala
                $check_user = "SELECT * FROM `client_room` WHERE `id_user` = :id";
                $check_user_stmt = $conn->prepare($check_user);
                $check_user_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                $check_user_stmt->execute();

                if($check_user_stmt->rowCount()):
                    $returnData = msg(0,401, "Ya estas registrado en una sala.");
                else:
                    // Valida que la room que manda el usuaroio exista
                    $check_room = "SELECT * FROM `room` WHERE `id_room` = :id";
                    $check_room_stmt = $conn->prepare($check_room);
                    $check_room_stmt->bindValue(':id', $id_room, PDO::PARAM_INT);
                    $check_room_stmt->execute();

                    if($check_room_stmt->rowCount()):
                        $row_r = $check_room_stmt->fetch(PDO::FETCH_ASSOC);

                        // Valida que este registrado como cliente
                        $check_client = "SELECT `name_barbershop`, `client_name`, `email` FROM `client_barbershop` WHERE `id_user` = :id";
                        $check_client_stmt = $conn->prepare($check_client);
                        $check_client_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                        $check_client_stmt->execute();

                        if($check_client_stmt->rowCount()):
                            // Registra el turno
                            $row = $check_client_stmt->fetch(PDO::FETCH_ASSOC);
        
                            $insert = "INSERT INTO `client_room` (`id_room`, `id_barbershop`, `id_user`, `full_name`, `date_time`) VALUES(:idRoom, :idB ,:id, :fullName, :date)";
                            $insert_stmt = $conn->prepare($insert);
                            $insert_stmt->bindValue(':idRoom', $row_r['id_room'], PDO::PARAM_INT);
                            $insert_stmt->bindValue(':idB', $row_r['id_barbershop'], PDO::PARAM_INT);
                            $insert_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                            $insert_stmt->bindValue(':fullName', $row['client_name'], PDO::PARAM_STR);
                            $insert_stmt->bindValue(':date', $date);
                            $insert_stmt->execute();
        
                            $returnData = msg(1,201,"Has ingresado correctamente a la sala!");
                        else:
                            $returnData = msg(0,401,"Actualmente no eres cliente de esta barberia, debido a eso no puedes ingresar");
                        endif;

                    else:
                        $returnData = msg(0,401,"La sala que ingresaste no existe!");
                    endif;
                endif;

    
            } catch(PDOException $e) {
                $returnData = msg(0,401,$e->getMessage());
            }
        endif;

    else:
        $returnData = msg(0,401,"No autorizado!");
    endif;

    echo json_encode($returnData);
?>