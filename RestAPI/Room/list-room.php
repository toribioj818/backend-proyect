<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()):
        $returnData = $auth->isAuth();

        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");

        if(!isset($data->name_barbershop)
            || empty($data->name_barbershop)
        ):
            $fields = ['fields' => ['name_barbershop']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        else:
            try {
                $nameBarbershop = $data->name_barbershop;
                
                // Busca si el nombre de la barberia es veridico
                $check_barbershop = "SELECT * FROM `barbershop` WHERE name_barbershop = :name";
                $check_barbershop_stmt = $conn->prepare($check_barbershop);
                $check_barbershop_stmt->bindValue(':name', $nameBarbershop, PDO::PARAM_STR);
                $check_barbershop_stmt->execute();

                if($check_barbershop_stmt->rowCount()):
                    // Busca que el usuario este registrado como cliente de esta barberia
                    $check_user = "SELECT * FROM `client_barbershop` WHERE `id_user` = :id";
                    $check_user_stmt = $conn->prepare($check_user);
                    $check_user_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                    $check_user_stmt->execute();

                    if($check_user_stmt->rowCount()):
                        $row = $check_user_stmt->fetch(PDO::FETCH_ASSOC);
                        // Busca la informacion de las salas para listarlas
                        $check_room = "SELECT `name_barbershop`, `owner`, `employee`, `description` FROM `room` WHERE `name_barbershop` = :name";
                        $check_room_stmt = $conn->prepare($check_room);
                        $check_room_stmt->bindValue(':name', $nameBarbershop, PDO::PARAM_STR);
                        $check_room_stmt->execute();

                        if($check_room_stmt->rowCount()):
                            $room = $check_room_stmt->fetchAll();

                            $returnData = [
                                "success" => 1,
                                "status" => 201,
                                "room" => $room 
                            ];
                        else:
                            $returnData = msg(0,401,"Actualmente no hay salas.");
                        endif;
                    else:
                        $returnData = msg(0,401,"Actualmente no esta registrado como cliente en esta barberia, por eso no es posible verificar las salas");
                    endif;
                else:
                    $returnData = msg(0,401,"El nombre que ha ingresado no esta registrado.");
                endif;


            } catch(PDOException $e) {
                $returnData = msg(0,401,$e->getMessage());
            }
        endif;

    else:
        $returnData = msg(0,401,"No autorizado!");
    endif;

    echo json_encode($returnData);
?>