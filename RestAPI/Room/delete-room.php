<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()):
        $returnData = $auth->isAuth();
        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
        $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

        if($category != "Barbero"):
            $returnData = msg(0,401,"No autorizado!");   
        else:
            $check_room = "SELECT * FROM `room` WHERE `id_user` = :id";
            $check_room_stmt = $conn->prepare($check_room);
            $check_room_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
            $check_room_stmt->execute();

            if($check_room_stmt->rowCount()):
                $query = "DELETE FROM `room` WHERE `id_user` = :id";
                $query_stmt = $conn->prepare($query);
                $query_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                $query_stmt->execute();

                $returnData = msg(1,201, "Sala removida correctamente!");
            else:
                $returnData = msg(0,401,"Actualmente no tienes una sala creada.");
            endif;
        endif;
    else:
        $returnData = msg(0,401,"No autorizado!");
    endif;

    echo json_encode($returnData);
?>