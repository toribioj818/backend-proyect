<?php 
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()):
        $returnData = $auth->isAuth();

        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
        $full_name = trim(json_encode($returnData['user']['full_name']), "\"..\"");
        $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

        $date = date("H:i:s");


        // Valida si es barbero
        if($category != "Barbero"):
            $returnData = msg(0,401,"No autorizado!");
        elseif (
            !isset($data->description)
            || empty($data->description)
        ) :
            $fields = ['fields' => ['description']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        else:
            try {
                $description = $data->description;

                // Valida que no tenga una sala creada
                $check_room = "SELECT * FROM `room` WHERE `owner` = :owner";
                $check_room_stmt = $conn->prepare($check_room);
                $check_room_stmt->bindValue(':owner', $full_name, PDO::PARAM_STR);
                $check_room_stmt->execute();

                if(!$check_room_stmt->rowCount()):
                    // Valida si es propietario o empleado de una barberia
                    $check_owner = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
                    $check_owner_stmt = $conn->prepare($check_owner);
                    $check_owner_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                    $check_owner_stmt->execute();

                    if($check_owner_stmt->rowCount()):
                        // Obten la informacion de la barberia
                        $row = $check_owner_stmt->fetch(PDO::FETCH_ASSOC);

                        // Crea la sala
                        $query = "INSERT INTO `room` (`id_barbershop`, `id_user`, `name_barbershop`, `owner`, `employee`, `description`, `date_time`) VALUES (:idBarbershop, :id,:name, :owner, 'NULL', :description, :date)";
                        $query_stmt = $conn->prepare($query);
                        $query_stmt->bindValue(':idBarbershop', $row['id_barbershop'], PDO::PARAM_INT);
                        $query_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                        $query_stmt->bindValue(':name', $row['name_barbershop'], PDO::PARAM_STR);
                        $query_stmt->bindValue(':owner', $row['owner'], PDO::PARAM_STR);
                        $query_stmt->bindValue(':description', $description, PDO::PARAM_STR);
                        $query_stmt->bindValue(':date', $date);
                        $query_stmt->execute();

                        $returnData = msg(1,201,"Sala creada correctamente");
                    else:
                        $returnData = msg(0,201,"Actualmente no tienes permiso para esta accion!");
                    endif;


                    // Si el usuario no es un propeitario , buscalo en empleados

                    $check_employee = "SELECT * FROM `employee_barbershop` WHERE `id_user` = :id";
                    $check_employee_stmt = $conn->prepare($check_employee);
                    $check_employee_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                    $check_employee_stmt->execute();

                    if($check_employee_stmt->rowCount()):
                        $row_e = $check_employee_stmt->fetch(PDO::FETCH_ASSOC);

                        $check_barbershop = "SELECT * FROM `barbershop` WHERE `id_barbershop` = :id";
                        $check_barbershop_stmt = $conn->prepare($check_barbershop);
                        $check_barbershop_stmt->bindValue(':id', $row_e['id_barbershop'], PDO::PARAM_INT);
                        $check_barbershop_stmt->execute();

                        if($check_barbershop_stmt->rowCount()):

                            $check_user = "SELECT * FROM `users` WHERE `id_user` = :id";
                            $check_user_stmt = $conn->prepare($check_user);
                            $check_user_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                            $check_user_stmt->execute();

                            $row_u = $check_user_stmt->fetch(PDO::FETCH_ASSOC);

                            if($check_user_stmt->rowCount()):
                                // Crea la sala
                                $query = "INSERT INTO `room` (`id_barbershop`, `id_user`, `name_barbershop`, `owner`, `employee`, `description`, `date_time`) VALUES (:idBarbershop, :id, :name, 'NULL', :employee, :description, :date)";
                                $query_stmt = $conn->prepare($query);
                                $query_stmt->bindValue(':idBarbershop', $row_e['id_barbershop'], PDO::PARAM_INT);
                                $query_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
                                $query_stmt->bindValue(':name', $row_e['name_barbershop'], PDO::PARAM_STR);
                                $query_stmt->bindValue(':employee', $row_u['full_name'], PDO::PARAM_STR);
                                $query_stmt->bindValue(':description', $description, PDO::PARAM_STR);
                                $query_stmt->bindValue(':date', $date);
                                $query_stmt->execute();

                                $returnData = msg(1,201,"Sala creada correctamente");
                            else:
                                $returnData = msg(0,401,"No existe");
                            endif;

                        else:
                            $returnData = msg(0,401,"No existe");
                        endif;
                    else:
                        
                    endif;
                else:
                    $returnData = msg(0,401,"Ya tienes una sala creada!");
                endif;

            } catch(PDOException $e) {
                $returnData = msg(0,401,$e->getMessage());
            }
        endif;
    else:
        $returnData = msg(0,201,"No autorizado!");
    endif;

    echo json_encode($returnData);
?>