<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: POST");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require __DIR__ . '/../Security/Class/database.php';
require __DIR__ . '/../Security/middlewares/Auth.php';

$allHeaders = getallheaders();
$db_connection = new database();
$conn = $db_connection->dbConnection();
$auth = new Auth($conn, $allHeaders);

$data = json_decode(file_get_contents("php://input"));
$returnData = [];

function msg($success, $status, $message, $extra = [])
{
  return array_merge([
    'success' => $success,
    'status' => $status,
    'message' => $message
  ], $extra);
}

if ($auth->isAuth()) {
  $returnData = $auth->isAuth();

  $idUser = trim(json_encode($returnData['user']['id_user']), "\"..\"");
  $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

  if ($category != "Barbero") :
    $returnData = msg(0, 201, "No autorizado!");
  else :
    try {
      // Valida que se propietario
      $check_owner = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
      $check_owner_stmt = $conn->prepare($check_owner);
      $check_owner_stmt->bindValue(':id', $idUser, PDO::PARAM_INT);
      $check_owner_stmt->execute();

      if ($check_owner_stmt->rowCount()) :
        $row = $check_owner_stmt->fetch(PDO::FETCH_ASSOC);
        $idBarbershop = $row['id_barbershop'];

        // Valida los datos enviados
        if (
          !isset($data->user_name)
          || empty($data->user_name)
        ) :
          $fields = ['fields' => ['user_name']];
          $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);
        else :
          $username = $data->user_name;

          if (strlen($username) < 3) :
            $returnData = msg(0, 401, "Lo que ha ingresado es muy corto.");
          else :
            $check_username = "SELECT * FROM `users` WHERE `user_name` = :name";
            $check_username_stmt = $conn->prepare($check_username);
            $check_username_stmt->bindValue(':name', $username, PDO::PARAM_STR);
            $check_username_stmt->execute();

            if ($check_username_stmt->rowCount()) :
              $row_user = $check_username_stmt->fetch(PDO::FETCH_ASSOC);
              $id_employee = $row_user['id_user'];

              // Valida que no sea un propietario
              $check_employee = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
              $check_employee_stmt = $conn->prepare($check_employee);
              $check_employee_stmt->bindValue(':id', $id_employee, PDO::PARAM_INT);
              $check_employee_stmt->execute();

              if ($check_employee_stmt->rowCount()) :
                $returnData = msg(0, 401, "Siendo propietario de una barberia, no puedes ser empleado de otra.");

              else :
                // Por ultimo valida si se registro como barbero
                $check_category = "SELECT * FROM `category_user` WHERE `id_user` = :id";
                $check_category_stmt = $conn->prepare($check_category);
                $check_category_stmt->bindValue(':id', $id_employee, PDO::PARAM_INT);
                $check_category_stmt->execute();

                if ($check_category_stmt->rowCount()) :
                  $row_employee = $check_category_stmt->fetch(PDO::FETCH_ASSOC);
                  $category_employee = $row_employee['category'];

                  if ($category_employee != "Barbero") :
                    $returnData = msg(0, 401, "El usuario no tiene la categoria necesaria.");

                  else :
                    // Validar que no sea un Empleado
                    $check_employee_barbershop = "SELECT * FROM `employee_barbershop` WHERE `id_user` = :id";
                    $check_employee_barbershop_stmt = $conn->prepare($check_employee_barbershop);
                    $check_employee_barbershop_stmt->bindValue(':id', $id_employee, PDO::PARAM_INT);
                    $check_employee_barbershop_stmt->execute();

                    if ($check_employee_barbershop_stmt->rowCount()) :
                      $returnData = msg(0, 401, "Actualmente el usuario ya es empleado.");
                    else :
                      // Si ya paso todos los proceso de verificacion... insertar
                      $insert_query = "INSERT INTO `employee_barbershop` (`id_barbershop`, `id_user`) VALUES (:idBarbershop, :idUser)";
                      $insert_query_stmt = $conn->prepare($insert_query);
                      $insert_query_stmt->bindValue(':idBarbershop', $idBarbershop, PDO::PARAM_INT);
                      $insert_query_stmt->bindValue(':idUser', $id_employee, PDO::PARAM_INT);
                      $insert_query_stmt->execute();

                      $returnData = msg(1, 201, "Empleado relacionado correctamente!");
                    endif;

                  endif;

                else :
                  $returnData = msg(0, 401, "No exite el usuario.");

                endif;

              endif;

            else :
              $returnData = msg(0, 401, "Usuario no encontrado!");
            endif;

          endif;

        endif;

      else :
        $returnData = msg(0, 401, "Actualmente no tienes una barberia registrada.");

      endif;
    } catch (PDOException $e) {
      $returnData = msg(0, 401, $e->getMessage());
    }

  endif;
} else {
  $returnData = msg(0, 401, "No autorizado!");
}

echo json_encode($returnData);
