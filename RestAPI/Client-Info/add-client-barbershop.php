<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    $data = json_decode(file_get_contents("php://input"));
    $returnData = [];

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }


    if($auth->isAuth()){

        $returnData = $auth->isAuth();

        // Informacion de usuario en sesion
        $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
        $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");
        $full_name = trim(json_encode($returnData['user']['full_name']), "\"..\"");
        $email = trim(json_encode($returnData['user']['email']), "\"..\"");

        if($_SERVER["REQUEST_METHOD"] != "POST"):
            $returnData = msg(0,404,'Pagina no autorizada!');

        elseif($category != "Cliente"):
            $returnData = msg(0,404,'Siendo barbero no puedes acceder a la informacion de otras barberias.');

        elseif(!isset($data->name_barbershop) || empty($data->name_barbershop)):
            $fields = ['fields' => ['name_barbershop']];
            $returnData = msg(0, 402, 'Por favor ingrese lo que se le pide!', $fields);

        else:
            $nameBarbershop = $data->name_barbershop;

            try{
                // Verfica si la barberia existe
                $check_barbershop = "SELECT * FROM `barbershop` WHERE `name_barbershop` = :name";
                $check_barbershop_stmt = $conn->prepare($check_barbershop);

                $check_barbershop_stmt->bindValue(':name', $nameBarbershop, PDO::PARAM_STR);
                $check_barbershop_stmt->execute();

                if($check_barbershop_stmt->rowCount()):
                    $row = $check_barbershop_stmt->fetch(PDO::FETCH_ASSOC);
                    $id_barbershop = $row['id_barbershop'];
                    $name = $row['name_barbershop'];

                    $insert_query = "INSERT INTO `client_barbershop` (`id_barbershop`, `id_user`, `name_barbershop`, `client_name`, `email`) VALUES (:idBarbershop, :idUser, :name, :client, :email)";
                    $insert_query_stmt = $conn->prepare($insert_query);

                    $insert_query_stmt->bindValue(':idBarbershop', $id_barbershop, PDO::PARAM_INT);
                    $insert_query_stmt->bindValue(':idUser', $id_user, PDO::PARAM_INT);
                    $insert_query_stmt->bindValue(':name', $name, PDO::PARAM_STR);
                    $insert_query_stmt->bindValue(':client', $full_name, PDO::PARAM_STR);
                    $insert_query_stmt->bindValue(':email', $email, PDO::PARAM_STR);

                    $insert_query_stmt->execute();


                    $returnData = msg(1,201,'Has ingresado a un nueva barberia!');
                else:
                    $returnData = msg(0,404,'La barberia no existe!');
                endif;

            } catch(PDOException $e) {
                $returnData = msg(0, 500,$e->getMessage());
            }

        endif;


    } else {
        $returnData = [
            'success' => 0,
            'status' => 401,
            'message' => "No autorizado"
        ];
    }

    echo json_encode($returnData);
?>
