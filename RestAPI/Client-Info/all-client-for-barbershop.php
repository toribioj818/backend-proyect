<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: access");
    header("Access-Control-Allow-Methods: POST");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    require __DIR__.'/../Security/Class/database.php';
    require __DIR__.'/../Security/middlewares/Auth.php';

    $allHeaders = getallheaders();
    $dbConnection = new database();
    $conn = $dbConnection->dbConnection();
    $auth = new Auth($conn, $allHeaders);

    function msg($success, $status, $message, $extra = []) {
        return array_merge([
            'success' => $success,
            'status' => $status,
            'message' => $message
        ], $extra);
    }

    if($auth->isAuth()) {
      $returnData = $auth->isAuth();

      $id_user = trim(json_encode($returnData['user']['id_user']), "\"..\"");
      $category = trim(json_encode($returnData['category_user']['category']), "\"..\"");

      if($category != "Barbero"):
        $returnData = msg(0,401,"No autorizado!");

      else:
        try {
          $check_barbershop = "SELECT * FROM `barbershop` WHERE `id_user` = :id";
          $check_barbershop_stmt = $conn->prepare($check_barbershop);
          $check_barbershop_stmt->bindValue(':id', $id_user, PDO::PARAM_INT);
          $check_barbershop_stmt->execute();

          if($check_barbershop_stmt->rowCount()):
            $row = $check_barbershop_stmt->fetch(PDO::FETCH_ASSOC);
            $idBarbershop = $row['id_barbershop'];
            $name_barbershop = $row['name_barbershop'];
            $owner = $row['owner'];

            $query = "SELECT * FROM `client_barbershop` WHERE `id_barbershop` = :id";
            $query_stmt = $conn->prepare($query);
            $query_stmt->bindValue(':id', $idBarbershop, PDO::PARAM_INT);
            $query_stmt->execute();

            if($query_stmt->rowCount()):
              while($info_client = $query_stmt->fetchAll()) {
                $returnData = [
                  "success" => 1,
                  "status" => 201,
                  "name_barbershop" => $name_barbershop,
                  "owner" => $owner,
                  "info_client" => $info_client
                ];
              }

            else:
              $returnData = msg(0,401,"No encontrado");
            endif;

          else:
            $returnData = msg(0,401,"Actualmente no eres propietario!");
          endif;

        } catch(PDOException $e) {
            $returnData = msg(0, 500,$e->getMessage());
        }
      endif;

    } else {
      $returnData = msg(0,401,"No autorizado!");
    }

    echo json_encode($returnData);
?>
